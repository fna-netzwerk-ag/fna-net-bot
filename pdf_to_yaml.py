"""
Script to clean up the pure IP list
Usage:
1. Paste the whole IP list contents into a plain text file.
2. Remove all table headers and other out-of-order lines.
3. Make sure only lines of the same format as the following examples are left:
    M000-M065 10.149.76.105 - 10.149.76.109 255.255.255.248 10.149.76.110 r076105 r076109
   or
    PLANNR-M0.53 10.149.76.121 - 10.149.76.125 255.255.255.248 10.149.76.126 r076121 r076125
4. Put in your own file paths down below.
5. Run script.
"""

import yaml

source = 'resources/list.txt'
target = 'resources/plaintext.yaml'

#  File line info split by space:
# [room-room, IP from, -, IP to, subnet mask, gateway, irrelevant, irrelevant]
# Important indexes: 0, 1, 3, 5

d = {}
with open(source) as f:
    for line in f:
        split = line.split(' ')

        # These rooms do not even exist
        if split[0].startswith('PLANNR'):
            continue

        short_room_name = split[0].split('-')[0]

        room_dict = {
            'ip_range': ' '.join([split[1], '-', split[3]]),
            'gateway': split[5],
            'alt_room_name': split[0],
        }

        d[short_room_name] = room_dict

        print(short_room_name, ':\n', room_dict)

with open(target, 'w+') as t:
    yaml.dump(d, t, default_flow_style=False)
