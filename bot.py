import logging
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    create_engine,
)
from sqlalchemy_utils import IPAddressType
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
import yaml
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater, CallbackQueryHandler


Base = declarative_base()


class BotAdmin(Base):
    __tablename__ = 'bot_admins'
    id = Column(Integer, primary_key=True, nullable=False)


class NetworkAdmin(Base):
    __tablename__ = 'network_admins'
    id = Column(Integer, primary_key=True, nullable=False)
    first_name = Column(String(64))
    last_name = Column(String(64))
    telegram_username = Column(String(32))


class RoomInfo(Base):
    __tablename__ = 'rooms'
    houseid = Column(String(1), primary_key=True, nullable=False)
    roomid = Column(Integer, primary_key=True, nullable=False)
    ip_min = Column(IPAddressType, nullable=False)
    ip_max = Column(IPAddressType, nullable=False)
    gateway = Column(IPAddressType, nullable=False)

    dns_server_one_id = Column(Integer, ForeignKey('dns_servers.id'), nullable=False)
    dns_server_two_id = Column(Integer, ForeignKey('dns_servers.id'))
    subnetmaskid = Column(Integer, ForeignKey('subnet_masks.id'), nullable=False)
    domainid = Column(Integer, ForeignKey('domains.id'), nullable=False)

    dns_server_one = relationship('DNSServer', foreign_keys="RoomInfo.dns_server_one_id")
    dns_server_two = relationship('DNSServer', foreign_keys="RoomInfo.dns_server_two_id")
    subnetmask = relationship('SubnetMask', foreign_keys="RoomInfo.subnetmaskid")
    domain = relationship('Domain', foreign_keys="RoomInfo.domainid")


class DNSServer(Base):
    __tablename__ = 'dns_servers'
    id = Column(Integer, primary_key=True)
    ip = Column(IPAddressType)


class SubnetMask(Base):
    __tablename__ = 'subnet_masks'
    id = Column(Integer, primary_key=True)
    mask = Column(IPAddressType)


class Domain(Base):
    __tablename__ = 'domains'
    id = Column(Integer, primary_key=True)
    name = Column(String(64))


def fill_db():
    with Session() as db_session:
        primary_dns = DNSServer(id=1, ip="10.156.33.53")
        secondary_dns = DNSServer(id=2, ip="129.187.5.1")
        subnet = SubnetMask(id=29, mask="255.255.255.248")
        domain = Domain(id=1, name="fna.swh.mhn.de")

        db_session.add(primary_dns)
        db_session.add(secondary_dns)
        db_session.add(subnet)
        db_session.add(domain)
        db_session.commit()

        with open('resources/plaintext.yaml') as file:
            rooms_yaml = yaml.safe_load(file)

        for room,data in rooms_yaml.items():
            house_nr = room[0]
            room_nr = room[1:]
            ip_range_split = data['ip_range'].split(' - ')
            db_room = RoomInfo(
                houseid=house_nr,
                roomid=room_nr,
                ip_min=ip_range_split[0],
                ip_max=ip_range_split[1],
                gateway=data['gateway'],
                dns_server_one_id=1,
                dns_server_two_id=2,
                subnetmaskid=29,
                domainid=1
            )

            db_session.add(db_room)
        db_session.commit()


def permission_denied_net_admin(update, context):
    """
    Send default permission denied message when authorizing as net admin failed.
    """
    msg = 'Permission denied. You must be a registered member of the networking group to do that.\n\n' \
          'If you are a member but not registered yet, register with /register <password>.'
    context.bot.send_message(chat_id=update.message.chat_id, text=msg, parse_mode='Markdown')


def permission_denied_bot_admin(update, context):
    """
    Send default permission denied message when authorizing as bot admin failed.
    """
    msg = 'Permission denied. You must be a bot admin to do that.'
    context.bot.send_message(chat_id=update.message.chat_id, text=msg, parse_mode='Markdown')


def start(update, context):
    """
    Send a message when the command /start is issued.
    """
    context.bot.send_message(chat_id=update.message.chat_id, text="Hello! Type /help to see what I can do!")


def help_message(update, context):
    """
    Send a help message when the command /help is issued.
    """
    help_msg = get_help_msg(update.message.from_user.id)
    context.bot.send_message(chat_id=update.message.chat_id, text=help_msg, parse_mode='Markdown')


def is_network_admin(user_id):
    with Session() as db_session:
        return db_session.query(NetworkAdmin).filter(NetworkAdmin.id == user_id).first() is not None


def is_bot_admin(user_id):
    with Session() as db_session:
        return db_session.query(BotAdmin).filter(BotAdmin.id == user_id).first() is not None


def ip(update, context):
    """
    Look up the IP one or more rooms
    """
    if not is_network_admin(update.effective_user.id):
        permission_denied_net_admin(update, context)
        return

    if len(context.args) < 1:
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text="The /ip command needs at least one room number as an argument!\n"
                                      "Example: /ip R123 R456")
    for room in context.args:
        house = room[0]
        room_nr = room[1:]

        with Session() as db_session:
            data = db_session.query(RoomInfo).filter(RoomInfo.houseid == house, RoomInfo.roomid == room_nr).first()

            if not data:
                text = "Couldn't find the requested room. If you believe this is a mistake, then please contact the network administrators or the maintainer of this bot."
                context.bot.send_message(chat_id=update.message.chat_id, text=text)
                continue

            text = f'Room: {room}\n' \
                f'IP Range: {data.ip_min} - {data.ip_max}\n'\
                f'Gateway: {data.gateway}\n'\
                f'Subnet Mask: {data.subnetmask.mask}\n'\
                f'Primary DNS: {data.dns_server_one.ip}\n'\
                f'Secondary DNS: {data.dns_server_two.ip}\n'\
                f'Domain: {data.domain.name}\n'

        context.bot.send_message(chat_id=update.message.chat_id, text=text)


def register(update, context):
    """
    Register yourself as network group member
    """
    sender = update.message.from_user
    # Check password
    if len(context.args) == 0:
        context.bot.send_message(chat_id=update.message.chat_id, text='Please supply a password.')
        return
    if context.args[0] != NETWORK_ADMIN_PASSWORD:
        context.bot.send_message(chat_id=update.message.chat_id, text='Wrong password.')
        return

    with Session() as db_session:
        if db_session.query(NetworkAdmin).filter(NetworkAdmin.id == sender.id).first():
            text = "You're already registered!"
            context.bot.send_message(chat_id=update.message.chat_id, text=text)
            return

        admin = NetworkAdmin(
            id=sender.id,
            first_name=sender.first_name,
            last_name=sender.last_name,
            telegram_username=sender.username
        )
        db_session.add(admin)
        db_session.commit()

    msg = 'Welcome to the networking group. Type /help to see your new powers!'
    context.bot.send_message(chat_id=update.message.chat_id, text=msg)


def reset_members(update, context):
    # Authorize bot admin only
    if not is_bot_admin(update.message.from_user.id):
        permission_denied_bot_admin(update, context)
        return

    # Save args temporarily
    context.user_data['reset_args'] = context.args

    # Confirmation buttons
    buttons = [[
        InlineKeyboardButton('Yes', callback_data='reset_members_Yes'),
        InlineKeyboardButton('No', callback_data='reset_members_No')
    ]]
    reply_markup = InlineKeyboardMarkup(buttons)
    warn_msg = 'This action will remove all registered members.\n' \
               'Are you sure you want to do that?'
    context.bot.send_message(chat_id=update.message.chat_id,
                             text=warn_msg, reply_markup=reply_markup)


def reset_confirmation(update, context):
    query = update.callback_query
    if query.data == 'reset_members_No':
        query.edit_message_text('Action cancelled.')
        del context.user_data['reset_args']
        return

    if query.data != 'reset_members_Yes':
        return


    with Session() as db_session:
        db_session.query(NetworkAdmin).delete(synchronize_session=False)
    query.edit_message_text('Member list reset successfully!')


def get_network_admins_list():
    admin_list = []
    with Session() as db_session:
        for admin in db_session.query(NetworkAdmin).all():
            name = admin.telegram_username
            if not name:
                name = admin.first_name
                if not name:
                    name = admin.last_name
            admin_list.append(name)
    return admin_list


def get_net_admins(update, context):
    """
    Get a list of net admin group members (members only atm)
    """
    if is_network_admin(update.message.from_user.id):
        admins = ', '.join(get_network_admins_list())
        msg = '*Registered network admin group members:*\n\n' + admins
        context.bot.send_message(chat_id=update.message.chat_id, text=msg, parse_mode='Markdown')
    else:
        permission_denied_net_admin(update, context)



def get_help_msg(user_id):
    """
    Determines access level of the user and returns a fitting help message
    """

    header = '*You can control me by sending these commands:*\n\n'

    # ------------------------------
    # Define your help messages here
    # ------------------------------
    ip = '_Look up the IP of a room_\n' \
        '/ip <room number>\n\n'

    register = '_Register as network group member_\n' \
                '/register <password>\n\n'

    reset_members = '_Reset all network admin group members and password_\n' \
                    '/reset\_members\n\n'

    get_net_admins = '_List all currently registered network group members_\n' \
                    '/get\_net\_admins\n\n'

    # Add your help message to its respective access level (only add ONCE!)
    bot_admin_only = [reset_members]
    net_admin_only = [ip, get_net_admins]
    free_for_all = [register]
    help_msg = header
    for msg in free_for_all:
        help_msg += msg
    if is_network_admin(user_id) and len(net_admin_only) > 0:
        help_msg += '\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\n' \
                    '🌐 *Network Admins Only* 🌐\n\n'
        for msg in net_admin_only:
            help_msg += msg
    if is_bot_admin(user_id) and len(bot_admin_only) > 0:
        help_msg += '\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\n' \
                    '🔑 *Bot Admins Only* 🔑\n\n'
        for msg in bot_admin_only:
            help_msg += msg
    return help_msg


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )

    with open("config.yaml") as config_file:
        config = yaml.safe_load(config_file)
        updater = Updater(token=config["token"], use_context=True)
        NETWORK_ADMIN_PASSWORD = config["admin_pw"]
        DB_ENGINE = create_engine(config["db_host"], pool_pre_ping=True)
        Session = sessionmaker(DB_ENGINE)
        Base.metadata.create_all(DB_ENGINE)

    fill_db()

    updater.dispatcher.add_handler(CommandHandler("start", start))
    updater.dispatcher.add_handler(CommandHandler("ip", ip))
    updater.dispatcher.add_handler(CommandHandler("get_net_admins", get_net_admins))
    updater.dispatcher.add_handler(CommandHandler("reset_members", reset_members))
    updater.dispatcher.add_handler(CallbackQueryHandler(reset_confirmation))
    updater.dispatcher.add_handler(CommandHandler("register", register))
    updater.dispatcher.add_handler(CommandHandler("help", help_message))

    updater.start_polling()
